// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "BossCharacter.generated.h"

UCLASS()
class FTGPROTOTYPE_API ABossCharacter : public ACharacter
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		float MaxHealth;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Boss")
		float Health;

	UPROPERTY()
		float RageThreshold = MaxHealth / 2;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		float Damage;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "Boss")
		bool isEnraged = false; //Phase 2 Trigger

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Boss")
		bool isDead = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		bool isStrafing = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		bool isCasting = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		bool isShooting = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		bool isLightAttack = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		bool isHeavyAttack = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Boss")
		bool isGroundPound = false;

	UFUNCTION(BlueprintCallable, Category = "Boss")
		virtual void ResetAttack(bool atkBool);
		
	UFUNCTION(BlueprintCallable, Category = "Boss")
		virtual void CalculateDamage(float dmg);


	virtual void CheckStatus(); //Checks to see if Boss should be Enraged or is it Dead

#if WITH_EDITOR
	//Editor-centric code for changing properties. This will make the changes from the editor permanent
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

public:
	// Sets default values for this character's properties
	ABossCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
