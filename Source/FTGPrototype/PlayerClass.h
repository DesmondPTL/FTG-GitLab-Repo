// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "PlayerClass.generated.h"

/*
UENUM(BlueprintType)
enum class EPlayerAnimState : uint8
{
	//! Default includes Idling and Movement
	AS_DEFAULT			UMETA(DisplayName = "Default"),
	//! Flinching is for when taking DMG
	AS_FLINCHING		UMETA(DisplayName = "Flinching"),
	//! Light Attack Animation. Will be changed to a combination later.
	AS_LIGHTATTACK		UMETA(DisplayName = "Light Attack"),
	//! Heavy Attack Animation. Will be changed later.
	AS_HEAVYATTACK		UMETA(DisplayName = "Heavy Attack"),
	//! Dash Animation. For when using Dash
	AS_DASH				UMETA(DisplayName = "Dashing"),
	//! Death Animation. Only plays on Death
	AS_DEAD				UMETA(DisplayName = "Dead")
};
*/

UCLASS()
class FTGPROTOTYPE_API APlayerClass : public ACharacter
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/*
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Player Class")
		EPlayerAnimState Player_ANIMSTATE;
	*/
public:	
	// Sets default values for this character's properties
	APlayerClass();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
};
