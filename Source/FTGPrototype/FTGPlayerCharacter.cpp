// Fill out your copyright notice in the Description page of Project Settings.

#include "FTGPrototype.h"
#include "FTGPlayerCharacter.h"


// Sets default values
AFTGPlayerCharacter::AFTGPlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*
	//! Components for the Character being set up in this Constructor
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 400.0f;
	CameraBoom->bUsePawnControlRotation = true;
	Cam = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Cam->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	*/
}

// Called when the game starts or when spawned
void AFTGPlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFTGPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFTGPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AFTGPlayerCharacter::IncrementBloodlust(float increment)
{
	if (!bPlayer_isBloodlustActive)
	{
		if (Player_Bloodlust + increment <= Player_MaxBloodlust)
		{
			Player_Bloodlust += increment;
		}
		else
		{
			Player_Bloodlust = Player_MaxBloodlust;
		}
	}
}

void AFTGPlayerCharacter::DecrementBloodlust(float decrement)
{
	if (bPlayer_isBloodlustActive)
	{
		if (Player_Bloodlust - decrement >= 0.0f)
		{
			Player_Bloodlust -= decrement;
		}
		else
		{
			Player_Bloodlust = 0.0f;
			bPlayer_isBloodlustActive = false;
		}
	}
}

void AFTGPlayerCharacter::RecoverHP(float val)
{
	if (Player_HP < Player_MaxHP)
	{
		Player_HP += val;
		if (Player_HP > Player_MaxHP) Player_HP = Player_MaxHP;
	}
}

void AFTGPlayerCharacter::RecoverMaxHP(float val)
{
	if (Player_MaxHP < 100.0f)
	{
		Player_MaxHP += val;

		if (Player_MaxHP > 100.0f) Player_MaxHP = 100.0f;

		Player_HP = Player_MaxHP;
	}
}

void AFTGPlayerCharacter::TakeDamage(float dmg, FVector source)
{
	if (!bPlayer_isIFrame)
	{
		Player_HP -= dmg;

		if (!source.IsZero())
		{
			FRotator RotationVal = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), source);
			FRotator LookAtRot = FRotator(0.0f, RotationVal.Yaw, RotationVal.Roll);
			SetActorRotation(LookAtRot);
		}

		bPlayer_isAttacking = false;
		/*
		if (!bPlayer_isKnockedBack)
		{
			Player_AnimState = EPlayerAnimState::AS_FLINCHING; 
		}
		else
		{
			Player_AnimState = EPlayerAnimState::AS_KNOCKBACK;
		}
		*/
	}

	CheckStatus();
}

void AFTGPlayerCharacter::CheckStatus()
{
	if (Player_HP <= 0.0f)
	{
		bPlayer_isDead = true;
		Player_AnimState = EPlayerAnimState::AS_DEAD;
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}

#if WITH_EDITOR
void AFTGPlayerCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Player_HP = Player_MaxHP;
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif