// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"


UCLASS(Blueprintable)
class FTGPROTOTYPE_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

//protected:
	/*
	UPROPERTY(EditAnywhere)
	USpringArmComponent* CameraBoom;
	UCameraComponent* Cam;
	*/
public:
	//Self Defined Properties
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		float MaxHealth = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		float Health = MaxHealth;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		float Damage = 5.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Bloodlust")
		float Bloodlust = 0.0f;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Player Bloodlust")
		float MaxBloodlust = 100.0f;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Player Character")
		bool bDead = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bFlinching = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bAttacking = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bLightAttack = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bHeavyAttack = false;

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		virtual void CalculateDamage(float dmg);

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		virtual void RecoverHP(float recoveryVal);

	UFUNCTION(BlueprintCallable, Category = "Player Bloodlust")
		virtual void IncrementBloodlust(float increment);

	UFUNCTION(BlueprintCallable, Category = "Player Bloodlust")
		virtual void DecrementBloodlust(float decrement);

	virtual void CheckDeath();

#if WITH_EDITOR
	//Editor-centric code for changing properties. This will make the changes from the editor permanent
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
public:
	// Sets default values for this character's properties
	APlayerCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
