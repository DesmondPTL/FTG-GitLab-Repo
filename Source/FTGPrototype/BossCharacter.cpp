// Fill out your copyright notice in the Description page of Project Settings.

#include "FTGPrototype.h"
#include "BossCharacter.h"


// Sets default values
ABossCharacter::ABossCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Health = MaxHealth;
}

// Called when the game starts or when spawned
void ABossCharacter::BeginPlay()
{
	Super::BeginPlay();
	RageThreshold = MaxHealth / 2;
}

// Called every frame
void ABossCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ABossCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABossCharacter::CalculateDamage(float dmg)
{
	Health += dmg;
	CheckStatus();
}

void ABossCharacter::CheckStatus()
{
	//! If Health is below Half & Not Dead
	if (Health <= 0)
	{
		if (!isDead)
		{
			isShooting = false;
			isLightAttack = false;
			isHeavyAttack = false;
			isGroundPound = false;
			isStrafing = false;
			isCasting = false;
			isDead = true;
		}
	}
	else if (Health <= RageThreshold)
	{
		if (!isDead && !isEnraged)
		{
			isEnraged = true;
		}
	}
}

void ABossCharacter::ResetAttack(bool atkBool)
{
	atkBool = false;
}

#if WITH_EDITOR
void ABossCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Health = MaxHealth;
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif