// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "FTGPlayerCharacter.generated.h"

UENUM(BlueprintType)
enum class EPlayerAnimState : uint8
{
	//! Default includes Idling and Movement
	AS_DEFAULT				UMETA(DisplayName = "Default"),
	//! Flinching is for when taking DMG
	AS_FLINCHING			UMETA(DisplayName = "Flinching"),
	//! Light Attack Animation. Will be changed to a combination later.
	AS_LIGHTATTACK			UMETA(DisplayName = "Light Attack"),
	//! Heavy Attack Animation. Will be changed later.
	AS_HEAVYATTACK			UMETA(DisplayName = "Heavy Attack"),
	//! Dash Animation. For when using Dash
	AS_DASH					UMETA(DisplayName = "Dashing"),
	//! Knockback Animation. For when hit by Ground Pound
	AS_KNOCKBACK			UMETA(DisplayName = "Knockback"),
	//! Recover from Knockback Animation
	AS_KNOCKBACKRECOVERY	UMETA(DisplayName = "Knockback Recovery"),
	//! Death Animation. Only plays on Death
	AS_DEAD				UMETA(DisplayName = "Dead")
};

UCLASS()
class FTGPROTOTYPE_API AFTGPlayerCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//UPROPERTY(EditAnywhere)
		//USpringArmComponent* CameraBoom;
		//UCameraComponent* Cam;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		EPlayerAnimState Player_AnimState;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		float Player_MaxHP = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		float Player_HP = Player_MaxHP;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Player Character")
		float Player_MaxBloodlust = 100.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		float Player_Bloodlust = 0.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bPlayer_isAttacking = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bPlayer_isDead = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bPlayer_isIFrame = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bPlayer_isBloodlustActive = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bPlayer_isKnockedBack = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Character")
		bool bPlayer_canDash = true;

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		virtual void IncrementBloodlust(float increment);

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		virtual void DecrementBloodlust(float decrement);

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		virtual void RecoverHP(float val);

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		virtual void RecoverMaxHP(float val);

	UFUNCTION(BlueprintCallable, Category = "Player Character")
		virtual void TakeDamage(float dmg, FVector source);

	virtual void CheckStatus();

#if WITH_EDITOR
		//Editor-centric code for changing properties. This will make the changes from the editor permanent
		virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif
public:
	// Sets default values for this character's properties
	AFTGPlayerCharacter();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	
};
