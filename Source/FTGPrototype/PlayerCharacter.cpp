// Fill out your copyright notice in the Description page of Project Settings.

#include "FTGPrototype.h"
#include "PlayerCharacter.h"


// Sets default values
APlayerCharacter::APlayerCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	/*
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f;
	CameraBoom->bUsePawnControlRotation = true;
	Cam = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Cam->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	*/
	//AutoPossessPlayer = EAutoReceiveInput::Player0;
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void APlayerCharacter::CalculateDamage(float dmg)
{
	Health += dmg;
	CheckDeath();
}

void APlayerCharacter::RecoverHP(float recoveryVal)
{
	if (Health < MaxHealth)
	{
		Health += recoveryVal;
		if (Health > MaxHealth) Health = MaxHealth;
	}
}

void APlayerCharacter::IncrementBloodlust(float increment)
{
	if (Bloodlust + increment <= MaxBloodlust)
	{
		Bloodlust += increment;
	}
	else
	{
		Bloodlust = MaxBloodlust;
	}
}

void APlayerCharacter::DecrementBloodlust(float decrement)
{
	if (Bloodlust - decrement >= 0.0f)
	{
		Bloodlust -= decrement;
	}
	else
	{
		Bloodlust = 0.0f;
	}
}

void APlayerCharacter::CheckDeath()
{
	if (Health <= 0)
	{
		bDead = true;
	}
	else
	{
		bDead = false;
	}
}

#if WITH_EDITOR
void APlayerCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Health = MaxHealth;
	Super::PostEditChangeProperty(PropertyChangedEvent);
}
#endif